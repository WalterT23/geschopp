from Util.util import Util
from Controlador.empleado_controller import ControladorEmpleado
from Controlador.cliente_controller import ControladorCliente
from Vista.empleado_view import EmpleadoView
from Vista.barril_view import BarrilView
from Vista.maquina_view import MaquinaView
from Vista.cliente_view import ClienteView
from Vista.servicio_view import ServicioView
import tkinter
class VistaMenu:
    def __init__(self):
        self.util = Util()
        self.x = ControladorEmpleado()
        self.p = ControladorCliente()
        self.empleado_view = EmpleadoView()
        self.barril_view = BarrilView()
        self.maquina_view = MaquinaView()
        self.cliente_view = ClienteView()
        self.servicio_view = ServicioView()


    def main_loop(self):
        '''Menu principal de la aplicacion'''

        #BOTONES DE LA VISTA
        def salir():
            ventana.destroy()

        def CurSelet(evt):
            value = str(mylistbox.get(mylistbox.curselection()))
            if (value == 'Agregar'):
                self.agregar_vista()
            elif (value == 'Listar'):
                self.listar_vista()
            elif (value == 'Buscar'):
                self.buscar_vista()
            elif (value == 'Servicio'):
                self.servicio_vista()


        ventana = tkinter.Tk()
        ventana.title("GESCHOPP - Gestion de Servicios de Chopp para Eventos")
        ventana.geometry("700x700")
        L1 = tkinter.Label(ventana, font='Arial', text="GESCHOPP - Gestion de Servicios de Chopp para Eventos")
        L1.place(bordermode='outside', height=30, x=50, y=10)

        L2 = tkinter.Label(ventana, font='Arial', text="POR FAVOR ELIJA LA TAREA A REALIZAR")
        L2.place(bordermode='outside', height=30, x=50, y=80)

        itemsforlistbox = ['Agregar', 'Listar', 'Buscar', 'Servicio']

        mylistbox = tkinter.Listbox(ventana, height=12, font=('times', 13))
        mylistbox.bind('<<ListboxSelect>>', CurSelet)
        mylistbox.place(x=45, y=110)

        for items in itemsforlistbox:
            mylistbox.insert('end', items)

        salir = tkinter.Button(ventana, text="Salir", command=salir)
        salir.place(bordermode='outside', height=40, width=100, x=140, y=400)

        ventana.mainloop()




    def agregar_vista(self):
        def salir():
            agregar.destroy()

        def volver():
            agregar.destroy()

        def CurSelet(evt):
            value = str(mylistbox.get(mylistbox.curselection()))
            if (value == 'Empleado'):
                self.empleado_view.cargar_empleado()
            elif (value == 'Barril'):
                self.barril_view.cargar_barril()
            elif (value == 'Maquina'):
                self.maquina_view.cargar_maquina()
            elif (value == 'Cliente'):
                self.cliente_view.cargar_cliente()

        agregar = tkinter.Tk()
        agregar.title("Agregar")
        agregar.geometry("700x700")
        L1 = tkinter.Label(agregar, font='Arial', text="ELIJA EL ITEM PARA AGREGAR")
        L1.place(bordermode='outside', height=50, x=100, y=10)

        itemsforlistbox = ['Empleado', 'Barril', 'Maquina', 'Cliente']

        mylistbox = tkinter.Listbox(agregar, height=12, width=50, font=('times', 13))
        mylistbox.bind('<<ListboxSelect>>', CurSelet)
        mylistbox.place(x=45, y=110)

        for items in itemsforlistbox:
            mylistbox.insert('end', items)

        volver = tkinter.Button(agregar, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
        salir = tkinter.Button(agregar, text="Salir", command=salir)
        salir.place(bordermode='outside', height=40, width=100, x=140, y=400)

        agregar.mainloop()




    def listar_vista(self):
        def salir():
            listar.destroy()

        def volver():
            listar.destroy()

        def CurSelet(evt):
            value = str(mylistbox.get(mylistbox.curselection()))
            if (value == 'Empleado'):
                self.empleado_view.listar_empleados()
            elif (value == 'Barril'):
                self.barril_view.listar_barril()
            elif (value == 'Maquina'):
                self.maquina_view.listar_maquinas()
            elif (value == 'Cliente'):
                self.cliente_view.listar_clientes()

        listar = tkinter.Tk()
        listar.title("Listar")
        listar.geometry("700x700")
        L1 = tkinter.Label(listar, font='Arial', text="ELIJA EL ITEM A LISTAR")
        L1.place(bordermode='outside', height=50, x=100, y=10)

        itemsforlistbox = ['Empleado', 'Barril', 'Maquina', 'Cliente']

        mylistbox = tkinter.Listbox(listar, height=12, width=50, font=('times', 13))
        mylistbox.bind('<<ListboxSelect>>', CurSelet)
        mylistbox.place(x=45, y=110)

        for items in itemsforlistbox:
            mylistbox.insert('end', items)

        volver = tkinter.Button(listar, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
        salir = tkinter.Button(listar, text="Salir", command=salir)
        salir.place(bordermode='outside', height=40, width=100, x=140, y=400)

        listar.mainloop()

    def buscar_vista(self):
        def salir():
            buscar.destroy()

        def volver():
            buscar.destroy()

        def CurSelet(evt):
            value = str(mylistbox.get(mylistbox.curselection()))
            if (value == 'Empleado'):
                self.empleado_view.solicitar_codigo()
            elif (value == 'Barril'):
                self.barril_view.solicitar_codigo()
            elif (value == 'Maquina'):
                self.maquina_view.solicitar_codigo()
            elif (value == 'Cliente'):
                self.cliente_view.buscar_cliente()
            elif (value == 'Volver'):
                self.main_loop()

        buscar = tkinter.Tk()
        buscar.title("Buscar")
        buscar.geometry("700x700")
        L1 = tkinter.Label(buscar, font='Arial', text="ELIJA EL ITEM A BUSCAR")
        L1.place(bordermode='outside', height=50, x=100, y=10)

        itemsforlistbox = ['Empleado', 'Barril', 'Maquina', 'Cliente']

        mylistbox = tkinter.Listbox(buscar, height=12, width=50, font=('times', 13))
        mylistbox.bind('<<ListboxSelect>>', CurSelet)
        mylistbox.place(x=45, y=110)

        for items in itemsforlistbox:
            mylistbox.insert('end', items)

        volver = tkinter.Button(buscar, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
        salir = tkinter.Button(buscar, text="Salir", command=salir)
        salir.place(bordermode='outside', height=40, width=100, x=140, y=400)

        buscar.mainloop()

    def servicio_vista(self):
        self.servicio_view.listar_servicios()

