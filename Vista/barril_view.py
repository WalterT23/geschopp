'''Vista para la carga de barril'''
from Util.util import Util
from Modelo.barril import Barril
from Controlador.barril_controller import ControladorBarril
import tkinter

class BarrilView:
    def __init__(self):
        self.util = Util()
        self.controller = ControladorBarril()
    def cargar_barril(self):
        barril = tkinter.Tk()
        barril.title("CARGAR BARRIL")
        barril.geometry("800x500")

        def volver():
            barril.destroy()

        def cargar():
            def cerrar_exp():
                barril.destroy()
                self.cargar_barril()

            def cerrar_alerta():
                alerta.destroy()

            try:
                capacidad_ = self.util.validar_entero(str(capacidad.get()), 1, "Capacidad:")
                marca_ = self.util.validar_cadena(str(marca.get()), True, "Marca:")
                precio_ = self.util.validar_entero(str(precio.get()), 1, "Precio:")

                contenedor = Barril(capacidad_, marca_, precio_)

                codigoBarril = self.controller.cargar_barril(contenedor)

            except Exception as e:
                alerta = tkinter.Message(barril, relief='raised',
                                         text='NO SE PUDO CARGAR EL BARRIL\nError: \n' + str(e), width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                ok.pack(side="bottom")
            else:
                alerta = tkinter.Message(barril, relief='raised', text='BARRIL '+codigoBarril+' CARGADO CON EXITO', width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")

        titulo = tkinter.Label(barril, font='Arial', text="DATOS DEL NUEVO BARRIL")
        titulo.place(bordermode='outside', height=25, width=350, x=100)
        # Etiquetas
        lbl_marca = tkinter.Label(barril, font='Arial', text="Marca", justify='left')
        lbl_marca.place(bordermode='outside', height=25, width=200, x=50, y=30)
        lbl_capacidad = tkinter.Label(barril, font='Arial', text="Capacidad")
        lbl_capacidad.place(bordermode='outside', height=25, width=200, x=50, y=55)
        lbl_precio = tkinter.Label(barril, font='Arial', text="Precio")
        lbl_precio.place(bordermode='outside', height=25, width=200, x=50, y=80)

        # Campos de Texto
        marca = tkinter.Entry(barril, font='times')
        marca.place(bordermode='outside', height=25, width=200, x=250, y=30)
        capacidad = tkinter.Entry(barril, font='times')
        capacidad.place(bordermode='outside', height=25, width=200, x=250, y=55)
        precio = tkinter.Entry(barril, font='times')
        precio.place(bordermode='outside', height=25, width=200, x=250, y=80)

        cargar = tkinter.Button(barril, text="Cargar", command=cargar)
        cargar.place(bordermode='outside', height=40, width=100, x=40, y=110)
        volver = tkinter.Button(barril, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=110)
        barril.mainloop()

    def solicitar_codigo(self):
        barril = tkinter.Tk()
        barril.title("BUSCAR BARRIL")
        barril.geometry("800x500")

        def volver():
            barril.destroy()

        def buscar():
            def cerrar_exp():
                barril.destroy()
                self.solicitar_codigo()

            dato = self.controller.buscar_barril(self.util.validar_cadena(str(codigo.get()), True))
            if dato != None and len(dato) > 0:
                lbl_codigo = tkinter.Label(barril, font='Arial', text="Código", justify='left')
                lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=55)
                lbl_marca = tkinter.Label(barril, font='Arial', text="Marca", justify='left')
                lbl_marca.place(bordermode='outside', height=25, width=300, x=50, y=80)
                lbl_capacidad = tkinter.Label(barril, font='Arial', text="Capacidad", justify='left')
                lbl_capacidad.place(bordermode='outside', height=25, width=300, x=50, y=105)
                lbl_precio = tkinter.Label(barril, font='Arial', text="Precio", justify='left')
                lbl_precio.place(bordermode='outside', height=25, width=300, x=50, y=130)

                codigo_result = tkinter.Label(barril, font='Arial', text=dato['codigob'], justify='left')
                codigo_result.place(bordermode='outside', height=25, width=300, x=350, y=55)
                marca_result = tkinter.Label(barril, font='Arial', text=dato['marca'], justify='left')
                marca_result.place(bordermode='outside', height=25, width=300, x=350, y=80)
                capacidad_result = tkinter.Label(barril, font='Arial', text=dato['capacidad'], justify='left')
                capacidad_result.place(bordermode='outside', height=25, width=300, x=350, y=105)
                precio_result = tkinter.Label(barril, font='Arial', text=dato['precio'], justify='left')
                precio_result.place(bordermode='outside', height=25, width=300, x=350, y=130)
            else:

                alerta = tkinter.Message(barril, relief='raised', text='Barril no encontrado', width=200)
                alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")



        titulo = tkinter.Label(barril, font='Arial', text="DATOS BARRIL")
        titulo.place(bordermode='outside', height=25, width=300, x=100)

        # Etiquetas
        lbl_codigo = tkinter.Label(barril, font='Arial', text="Código del Barril", justify='left')
        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=30)

        # Campos de Texto
        codigo = tkinter.Entry(barril, font='times')
        codigo.place(bordermode='outside', height=25, width=300, x=350, y=30)

        buscar = tkinter.Button(barril, text="Buscar", command=buscar)
        buscar.place(bordermode='outside', height=40, width=100, x=40, y=210)
        volver = tkinter.Button(barril, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=210)
        barril.mainloop()

    def listar_barril(self):
        def cerrar_exp():
            barril.destroy()
            self.cargar_barril()

        barril = tkinter.Tk()
        barril.title("LISTADO DE BARRILES")
        barril.geometry("1000x500")

        def volver():
            barril.destroy()

        mylistbox = tkinter.Listbox(barril, height=20, width=100, font=('times', 13))
        mylistbox.place(x=32, y=110)
        fun = self.controller.listar_barril()
        if fun != None and len(fun) != 0:
            for values in fun:
                mylistbox.insert('end', '* Codigo: ' + values.codigob + ', Marca: ' + values.marca + ', Capacidad: '
                                 + str(values.capacidad) + ', Precio: ' + str(values.precio))
            titulo = tkinter.Label(barril, font='Arial', text="LISTADO DE BARRILES")
            titulo.place(bordermode='outside', height=25, width=600, x=100, y=30)
            volver = tkinter.Button(barril, text="Volver", command=volver)
            volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
            barril.mainloop()
        else:
            alerta = tkinter.Message(barril, relief='raised',
                                     text='NO EXISTEN REGISTROS ', width=200)
            alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
            ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
            ok.pack(side="bottom")





