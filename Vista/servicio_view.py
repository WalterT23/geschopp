#vista orden de analisis
from Controlador.barril_controller import ControladorBarril
from Controlador.cliente_controller import ControladorCliente
from Controlador.maquina_controller import ControladorMaquina
from Modelo.servicio_consignacion import ServicioConsignacion
from Modelo.servicio_normal import ServicioNormal
from Util.util import Util
from Vista.cliente_view import ClienteView
from Controlador.servicio_controller import ServicioController
import tkinter
import tkinter.ttk as ttk
from tkcalendar import DateEntry




class ServicioView:
    geometric_definicion = "1000x800"
    def __init__(self):
        self.util = Util()
        self.view = ClienteView()
        self.controller = ServicioController()
        self.ctrlClient = ControladorCliente()
        self.ctrlMaquina = ControladorMaquina()
        self.ctrlBarril = ControladorBarril()


    def listar_servicios(self):
        def salir():
            ventanaServicio.destroy()

        def CurSelet(evt):
            value = str(mylistbox.get(mylistbox.curselection()))
            if (value == 'Agendar'):
                self.agendar_vista()
            elif (value == 'Listar'):
                self.listar_vista()
            elif (value == 'Buscar'):
                self.buscar_vista()
            elif (value == 'Finalizar'):
                self.finalizar_vista()
            #elif (value == 'Eliminar'):
                #self.controller.eliminar_todo()

        ventanaServicio = tkinter.Tk()
        ventanaServicio.title("GESCHOPP - Gestion de Servicios de Chopp para Eventos")
        ventanaServicio.geometry(self.geometric_definicion)
        L1 = tkinter.Label(ventanaServicio, font='Arial', text="GESCHOPP - Gestion de Servicios de Chopp para Eventos")
        L1.place(bordermode='outside', height=30, x=50, y=10)
        L2 = tkinter.Label(ventanaServicio, font='Arial', text="EL SERVICIO DE CHOPP MAS FRIO DEL PAIS")
        L2.place(bordermode='outside', height=30, x=50, y=80)

        itemsforlistbox = ['Agendar', 'Listar', 'Buscar', 'Finalizar']
            #, 'Eliminar']

        mylistbox = tkinter.Listbox(ventanaServicio, height=12, font=('times', 13))
        mylistbox.bind('<<ListboxSelect>>', CurSelet)
        mylistbox.place(x=45, y=110)

        for items in itemsforlistbox:
            mylistbox.insert('end', items)

        salir = tkinter.Button(ventanaServicio, text="Salir", command=salir)
        salir.place(bordermode='outside', height=40, width=100, x=140, y=400)

        ventanaServicio.mainloop()


    def agendar_vista(self):
        agendarVentana = tkinter.Tk()
        agendarVentana.title("AGENDAR SERVICIO")
        agendarVentana.geometry(self.geometric_definicion)

        def volver():
            agendarVentana.destroy()

        def agendar():
            def cerrar_exp():
                agendarVentana.destroy()

            def cerrar_alerta():
                alerta.destroy()

            def obtener_primer_elemento(cadena):
                return cadena.split('-')[0]

            try:
                tipoServicio_ = self.util.validar_cadena(str(tipo_servicio.get()), True, "Servicio:")
                cedula_cliente_ = obtener_primer_elemento(self.util.validar_cadena(str(cliente.get()), True, "Cliente:"))
                cedula_cliente_ = cedula_cliente_.strip()
                fecha_entrega_ = self.util.validar_fecha(str(fecha_entrega.get_date().strftime('%d/%m/%Y')), "Fecha entrega:")
                fecha_retiro_ = self.util.validar_fecha(str(fecha_retiro.get_date().strftime('%d/%m/%Y')), "Fecha retiro:")
                codigo_barril_ = obtener_primer_elemento(self.util.validar_cadena(str(barril.get()), True, "Barril:"))
                codigo_barril_ = codigo_barril_.strip()
                codigo_maquina_ = self.util.validar_entero(str(maquina.get()), 1, "Maquina:")
                cantidad_ = self.util.validar_entero(str(cantidad.get()), 1, "Cantidad:")


                if tipoServicio_ == 'Normal':
                    contenedor = ServicioNormal(cedula_cliente_, fecha_entrega_, fecha_retiro_, codigo_maquina_, codigo_barril_)
                    contenedor.cantidad_barril = cantidad_
                    codigoServicio = self.controller.cargar_agendamiento(contenedor)
                else:
                    cantidad_consignacion_ = self.util.validar_entero(str(cantidad_consignacion.get()), 0, "Cantidad consignacion:")
                    contenedor = ServicioConsignacion(cedula_cliente_, fecha_entrega_, fecha_retiro_, codigo_maquina_, codigo_barril_)
                    contenedor.cantidad_barril = cantidad_
                    contenedor.cantidad_consignacion = cantidad_consignacion_
                    codigoServicio = self.controller.cargar_agendamiento(contenedor)

                    """alerta = tkinter.Message(agendarVentana, relief='raised',
                                             text=str(cantidad_consignacion_), width=300)
                    alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                    ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                    ok.pack(side="bottom")"""
            except Exception as e:
                alerta = tkinter.Message(agendarVentana, relief='raised',
                                         text='NO SE PUDO AGENDAR EL SERVICIO\nError: \n' + str(e), width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                ok.pack(side="bottom")
            else:
                alerta = tkinter.Message(agendarVentana, relief='raised',
                                         text='SERVICIO Nº ' + str(codigoServicio) + ' CARGADO CON EXITO', width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")

        titulo = tkinter.Label(agendarVentana, font='Arial', text="AGENDAR SERVICIO CHOPP")
        titulo.place(bordermode='outside', height=25, width=350, x=100)
        # Etiquetas
        lbl_tipo_servicio = tkinter.Label(agendarVentana, font='Arial', text="Tipo de Servicio", justify='left')
        lbl_tipo_servicio.place(bordermode='outside', height=25, width=200, x=50, y=30)
        lbl_cliente = tkinter.Label(agendarVentana, font='Arial', text="Cedula cliente")
        lbl_cliente.place(bordermode='outside', height=25, width=200, x=50, y=55)
        lbl_fecha_entrega = tkinter.Label(agendarVentana, font='Arial', text="Fecha entrega")
        lbl_fecha_entrega.place(bordermode='outside', height=25, width=200, x=50, y=80)
        lbl_fecha_retiro = tkinter.Label(agendarVentana, font='Arial', text="Fecha retiro")
        lbl_fecha_retiro.place(bordermode='outside', height=25, width=200, x=50, y=105)
        lbl_maquina = tkinter.Label(agendarVentana, font='Arial', text="Codigo maquina")
        lbl_maquina.place(bordermode='outside', height=25, width=200, x=50, y=130)
        lbl_barril = tkinter.Label(agendarVentana, font='Arial', text="Codigo barril")
        lbl_barril.place(bordermode='outside', height=25, width=200, x=50, y=155)
        lbl_cantidad = tkinter.Label(agendarVentana, font='Arial', text="Cantidad barril")
        lbl_cantidad.place(bordermode='outside', height=25, width=200, x=50, y=180)
        lbl_cantidad_consignacion = tkinter.Label(agendarVentana, font='Arial', text="Barril consignacion")
        lbl_cantidad_consignacion.place(bordermode='outside', height=25, width=200, x=50, y=205)

        # Campos de Texto
        tipoSer = ['Normal','Consignacion']
        tipo_servicio = ttk.Combobox(agendarVentana, value=tipoSer, state='readonly')
        tipo_servicio.place(bordermode='outside', height=25, width=200, x=250, y=30)

        listClient = self.ctrlClient.listar_clientes()
        lista_cedulas = [f"{objeto.cedula} - {objeto.nombre} {objeto.apellido}" for objeto in listClient]
        cliente = ttk.Combobox(agendarVentana, value= lista_cedulas, state='readonly')
        cliente.place(bordermode='outside', height=25, width=200, x=250, y=55)
        #cliente = tkinter.Entry(agendarVentana, font='times')
        #cliente.place(bordermode='outside', height=25, width=200, x=250, y=55)

        #fecha_entrega = tkinter.Entry(agendarVentana, font='times')
        #fecha_entrega.place(bordermode='outside', height=25, width=200, x=250, y=80)
        fecha_entrega = DateEntry(agendarVentana, date_pattern='dd/mm/yyyy')
        fecha_entrega.place(bordermode='outside', height=25, width=200, x=250, y=80)
        #fecha_retiro = tkinter.Entry(agendarVentana, font='times')
        #fecha_retiro.place(bordermode='outside', height=25, width=200, x=250, y=105)
        fecha_retiro = DateEntry(agendarVentana, date_pattern='dd/mm/yyyy')
        fecha_retiro.place(bordermode='outside', height=25, width=200, x=250, y=105)

        listMaquina = self.ctrlMaquina.listar_maquina()
        lista_maquinas = [objeto.codigom for objeto in listMaquina]
        maquina = ttk.Combobox(agendarVentana, value=lista_maquinas, state='readonly')
        maquina.place(bordermode='outside', height=25, width=200, x=250, y=130)
        #maquina = tkinter.Entry(agendarVentana, font='times')
        #maquina.place(bordermode='outside', height=25, width=200, x=250, y=130)

        listBarril = self.ctrlBarril.listar_barril()
        lista_barril = [f"{objeto.codigob} - {objeto.marca}" for objeto in listBarril]
        barril = ttk.Combobox(agendarVentana, value=lista_barril, state='readonly')
        barril.place(bordermode='outside', height=25, width=200, x=250, y=155)
        #barril = tkinter.Entry(agendarVentana, font='times')
        #barril.place(bordermode='outside', height=25, width=200, x=250, y=155)
        cantidad = tkinter.Entry(agendarVentana, font='times')
        cantidad.place(bordermode='outside', height=25, width=200, x=250, y=180)
        cantidad_consignacion = tkinter.Entry(agendarVentana, font='times')
        cantidad_consignacion.place(bordermode='outside', height=25, width=200, x=250, y=205)

        cargar = tkinter.Button(agendarVentana, text="Cargar", command=agendar)
        cargar.place(bordermode='outside', height=40, width=100, x=40, y=235)
        volver = tkinter.Button(agendarVentana, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=235)
        agendarVentana.mainloop()

    def listar_vista(self):
        listaVentana = tkinter.Tk()
        listaVentana.title("LISTAR AGENDAMIENTOS DE SERVICIO")
        listaVentana.geometry(self.geometric_definicion)

        def volver():
            listaVentana.destroy()

        def cerrar_alerta():
            alerta.destroy()

        titulo = tkinter.Label(listaVentana, font='Arial', text="AGENDAR SERVICIO CHOPP")
        titulo.place(bordermode='outside', height=25, width=350, x=100)

        mylistbox = tkinter.Listbox(listaVentana, height=20, width=100, font=('times', 11))
        mylistbox.place(x=32, y=120)
        milista = self.controller.listar_agendamientos()
        if milista != None and len(milista) != 0:
            for values in milista:
                mylistbox.insert('end',
                                 '* Codigo Servicio: ' + str(values.idservicio)
                                 + ', Cliente: ' + str(values.cliente)
                                 + ', Fecha entrega: ' + str(values.fecha_entrega)
                                 + ', Estado: ' + str(values.estado_servicio)
                                 + ', Tipo: ' + str(values.tipo))
            titulo = tkinter.Label(listaVentana, font='Arial', text="LISTADO DE SERVICIOS AGENDADOS")
            titulo.place(bordermode='outside', height=25, width=600, x=100, y=30)
            volver = tkinter.Button(listaVentana, text="Volver", command=volver)
            volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
            listaVentana.mainloop()
        else:
            alerta = tkinter.Message(listaVentana, relief='raised',
                                     text='NO EXISTEN REGISTROS ', width=200)
            alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
            ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
            ok.pack(side="bottom")


        volver = tkinter.Button(listaVentana, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=210)
        listaVentana.mainloop()

    def buscar_vista(self):
        buscarVentana = tkinter.Tk()
        buscarVentana.title("BUSCAR AGENDAMIENTO")
        buscarVentana.geometry(self.geometric_definicion)

        def volver():
            buscarVentana.destroy()

        def buscar():
            #def cerrar_exp():
                #buscarVentana.destroy()

            def cerrar_alerta():
                alerta.destroy()

            try:
                codigo_ = self.util.validar_entero(str(codigo.get()), None, "Codigo:")
                #cedula_ = self.util.validar_cadena(str(cedula.get()), False, "Cedula:")
                if codigo_ == None:# and cedula_ == None:
                    self.util.validar_cadena(None, True, "Codigo:")
                if codigo_ != None:
                    dato = self.controller.buscar_servicio_codigo(codigo_)
                    if dato != None and dato != {}:
                        lbl_codigo = tkinter.Label(buscarVentana, font='Arial', text="Codigo Servicio", justify='left')
                        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=80)
                        lbl_cliente = tkinter.Label(buscarVentana, font='Arial', text="Cliente", justify='left')
                        lbl_cliente.place(bordermode='outside', height=25, width=300, x=50, y=105)
                        lbl_estado = tkinter.Label(buscarVentana, font='Arial', text="Estado", justify='left')
                        lbl_estado.place(bordermode='outside', height=25, width=300, x=50, y=130)
                        lbl_fecha_entrega = tkinter.Label(buscarVentana, font='Arial', text="Fecha entrega",
                                                          justify='left')
                        lbl_fecha_entrega.place(bordermode='outside', height=25, width=300, x=50, y=155)
                        lbl_fecha_retiro = tkinter.Label(buscarVentana, font='Arial', text="Fecha retiro",
                                                         justify='left')
                        lbl_fecha_retiro.place(bordermode='outside', height=25, width=300, x=50, y=180)
                        lbl_maquina = tkinter.Label(buscarVentana, font='Arial', text="Maquina", justify='left')
                        lbl_maquina.place(bordermode='outside', height=25, width=300, x=50, y=205)
                        lbl_barril = tkinter.Label(buscarVentana, font='Arial', text="Barril", justify='left')
                        lbl_barril.place(bordermode='outside', height=25, width=300, x=50, y=230)
                        lbl_cantidad = tkinter.Label(buscarVentana, font='Arial', text="Cantidad barril",
                                                     justify='left')
                        lbl_cantidad.place(bordermode='outside', height=25, width=300, x=50, y=255)

                        lbl_estado_consig = tkinter.Label(buscarVentana, font='Arial', text="Estado consignacion",
                                                          justify='left')
                        lbl_estado_consig.place(bordermode='outside', height=25, width=300, x=50, y=280)
                        lbl_cantidad_consig = tkinter.Label(buscarVentana, font='Arial', text="Barril a consignacion",
                                                     justify='left')
                        lbl_cantidad_consig.place(bordermode='outside', height=25, width=300, x=50, y=305)


                        lbl_costo = tkinter.Label(buscarVentana, font='Arial', text="Costo servicio",
                                                  justify='left')
                        lbl_costo.place(bordermode='outside', height=25, width=300, x=50, y=330)

                        codigo_result = tkinter.Label(buscarVentana, font='Arial', text=dato.idservicio, justify='left')
                        codigo_result.place(bordermode='outside', height=25, width=300, x=350, y=80)
                        cliente_result = tkinter.Label(buscarVentana, font='Arial', text=dato.cliente, justify='left')
                        cliente_result.place(bordermode='outside', height=25, width=300, x=350, y=105)
                        estado_result = tkinter.Label(buscarVentana, font='Arial', text=dato.estado_servicio,
                                                      justify='left')
                        estado_result.place(bordermode='outside', height=25, width=300, x=350, y=130)
                        fecha_entrega_result = tkinter.Label(buscarVentana, font='Arial', text=dato.fecha_entrega,
                                                             justify='left')
                        fecha_entrega_result.place(bordermode='outside', height=25, width=300, x=350, y=155)
                        fecha_retiro_result = tkinter.Label(buscarVentana, font='Arial', text=dato.fecha_retiro,
                                                            justify='left')
                        fecha_retiro_result.place(bordermode='outside', height=25, width=300, x=350, y=180)
                        maquina_result = tkinter.Label(buscarVentana, font='Arial', text=dato.maquina['codigom'],
                                                       justify='left')
                        maquina_result.place(bordermode='outside', height=25, width=300, x=350, y=205)
                        barril_result = tkinter.Label(buscarVentana, font='Arial', text=dato.barril['codigob'],
                                                      justify='left')
                        barril_result.place(bordermode='outside', height=25, width=300, x=350, y=230)
                        cantidad_result = tkinter.Label(buscarVentana, font='Arial', text=dato.cantidad_barril,
                                                        justify='left')
                        cantidad_result.place(bordermode='outside', height=25, width=300, x=350, y=255)

                        estado_result_consig = tkinter.Label(buscarVentana, font='Arial', text='-',
                                                             justify='left')
                        estado_result_consig.place(bordermode='outside', height=25, width=300, x=350, y=280)
                        cantidad_result_consig = tkinter.Label(buscarVentana, font='Arial',
                                                               text=0,
                                                               justify='left')
                        cantidad_result_consig.place(bordermode='outside', height=25, width=300, x=350, y=305)

                        if dato.tipo == 'CONSIGNACION':
                            estado_result_consig.config(text=dato.estado_consignacion)
                            cantidad_result_consig.config(text=dato.cantidad_consignacion)
                        costo_result = tkinter.Label(buscarVentana, font='Arial', text=f"{dato.costo_total:,.0f}".replace(",", "."),
                                                     justify='left')
                        costo_result.place(bordermode='outside', height=25, width=300, x=350, y=330)
                        #volver = tkinter.Button(buscarVentana, text="Volver", command=cerrar_exp)
                        #volver.place(bordermode='outside', height=40, width=100, x=500, y=400)
                    else:
                        alerta = tkinter.Message(buscarVentana, relief='raised', text='No se encontraron registros',
                                                 width=200)
                        alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                        ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                        ok.pack(side="bottom")
                """else:
                    lista = self.controller.buscar_servicio_cedula(cedula_)
                    mylistbox = tkinter.Listbox(buscarVentana, height=25, width=100, font=('times', 11))
                    mylistbox.place(x=32, y=120)
                    if lista != None and len(lista) != 0:
                        for values in lista:
                            mylistbox.insert('end',
                                             '* Codigo Servicio: ' + str(values.idservicio)
                                             + ', Cliente: ' + str(values.cliente)
                                             + ', Fecha entrega: ' + str(values.fecha_entrega)
                                             + ', Estado: ' + str(values.estado_servicio)
                                             + ', Tipo: ' + str(values.tipo))
                            # + ', Maquina: ' + str(values.maquina['codigom'])
                            # + ', Barril: ' + str(values.barril['codigob'])
                            # + ', Cantidad: ' + str(values.cantidad_barril))
                        #volver = tkinter.Button(buscarVentana, text="Volver", command=cerrar_exp)
                        #volver.place(bordermode='outside', height=40, width=100, x=40, y=400)"""
            except Exception as e:
                alerta = tkinter.Message(buscarVentana, relief='raised',
                                         text='NO SE PUDO BUSCAR EL SERVICIO\nError: \n' + str(e), width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                ok.pack(side="bottom")




        titulo = tkinter.Label(buscarVentana, font='Arial', text="DATOS DEL SERVICIO AGENDADO")
        titulo.place(bordermode='outside', height=25, width=300, x=100)

        # Etiquetas
        lbl_codigo = tkinter.Label(buscarVentana, font='Arial', text="Código del Servicio", justify='left')
        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=30)
        #lbl_cedula = tkinter.Label(buscarVentana, font='Arial', text="Cedula cliente", justify='left')
        #lbl_cedula.place(bordermode='outside', height=25, width=300, x=50, y=55)

        # Campos de Texto
        codigo = tkinter.Entry(buscarVentana, font='times')
        codigo.place(bordermode='outside', height=25, width=300, x=350, y=30)
        #cedula = tkinter.Entry(buscarVentana, font='times')
        #cedula.place(bordermode='outside', height=25, width=300, x=350, y=55)

        buscar = tkinter.Button(buscarVentana, text="Buscar", command=buscar)
        buscar.place(bordermode='outside', height=40, width=100, x=140, y=400)
        volver = tkinter.Button(buscarVentana, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=240, y=400)
        buscarVentana.mainloop()

    def finalizar_vista(self):
        finalizarVentana = tkinter.Tk()
        finalizarVentana.title("FINALIZAR AGENDAMIENTO")
        finalizarVentana.geometry(self.geometric_definicion)
        def volver():
            finalizarVentana.destroy()
        def buscar():
            def cerrar_exp():
                finalizarVentana.destroy()

            def finalizar_operacion():
                def cerrar_alerta2():
                    alerta.destroy()
                    cerrar_exp()

                id = None
                if dato.estado_servicio == 'FINALIZADA':
                    alerta = tkinter.Message(finalizarVentana, relief='raised', text='No se puede finalizar el servicio ya se encuentra finalizado', justify='left',
                                             width=200)
                    alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                    ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta2)
                    ok.pack(side="bottom")
                else:
                    id = self.controller.finalizar_ope(dato, codigo_)
                if id != None:
                    alerta = tkinter.Message(finalizarVentana, relief='raised', text='Servicio finalizado con exito',
                                             width=200)
                    alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                    ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta2)
                    ok.pack(side="bottom")

            def cerrar_alerta():
                alerta.destroy()



            def actualizar_costo_result(event):
                seleccion = utilizo_consignacion.get()
                dato.consig_ejecutado = seleccion
                if seleccion == 'SI':
                    dato.costo_total = (dato.cantidad_barril + dato.cantidad_consignacion) * dato.barril['precio']
                else:
                    dato.costo_total = dato.cantidad_barril * dato.barril['precio']

                costo_result.config(text=f"{dato.costo_total:,.0f}".replace(",", "."))

            try:
                codigo_ = self.util.validar_entero(str(codigo.get()), 0, "Codigo:")

                if codigo_ != None:
                    dato = self.controller.calcular_servicio(codigo_)
                    if dato != None and dato != {}:
                        lbl_codigo = tkinter.Label(finalizarVentana, font='Arial', text="Codigo Servicio",
                                                   justify='left')
                        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=80)
                        lbl_cliente = tkinter.Label(finalizarVentana, font='Arial', text="Cliente", justify='left')
                        lbl_cliente.place(bordermode='outside', height=25, width=300, x=50, y=105)
                        lbl_estado = tkinter.Label(finalizarVentana, font='Arial', text="Estado", justify='left')
                        lbl_estado.place(bordermode='outside', height=25, width=300, x=50, y=130)
                        lbl_fecha_entrega = tkinter.Label(finalizarVentana, font='Arial', text="Fecha entrega",
                                                          justify='left')
                        lbl_fecha_entrega.place(bordermode='outside', height=25, width=300, x=50, y=155)
                        lbl_fecha_retiro = tkinter.Label(finalizarVentana, font='Arial', text="Fecha retiro",
                                                         justify='left')
                        lbl_fecha_retiro.place(bordermode='outside', height=25, width=300, x=50, y=180)
                        lbl_maquina = tkinter.Label(finalizarVentana, font='Arial', text="Maquina", justify='left')
                        lbl_maquina.place(bordermode='outside', height=25, width=300, x=50, y=205)
                        lbl_barril = tkinter.Label(finalizarVentana, font='Arial', text="Barril", justify='left')
                        lbl_barril.place(bordermode='outside', height=25, width=300, x=50, y=230)
                        lbl_cantidad = tkinter.Label(finalizarVentana, font='Arial', text="Cantidad barril",
                                                     justify='left')
                        lbl_cantidad.place(bordermode='outside', height=25, width=300, x=50, y=255)

                        lbl_cantidad_consig = tkinter.Label(finalizarVentana, font='Arial',
                                                            text="Barril a consignacion",
                                                            justify='left')
                        lbl_utilizado_consig = tkinter.Label(finalizarVentana, font='Arial',
                                                             text="Consignacion ejecutada",
                                                             justify='left')

                        #lbl_estado_consig = tkinter.Label(finalizarVentana, font='Arial', text="Estado consignacion", justify='left')
                        #lbl_estado_consig.place(bordermode='outside', height=25, width=300, x=50, y=305)
                        lbl_cantidad_consig.place(bordermode='outside', height=25, width=300, x=50, y=280)
                        lbl_utilizado_consig.place(bordermode='outside', height=25, width=300, x=50, y=305)

                        lbl_costo = tkinter.Label(finalizarVentana, font='Arial', text="Costo Final",
                                                  justify='left')
                        lbl_costo.place(bordermode='outside', height=25, width=300, x=50, y=360)

                        codigo_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.idservicio,
                                                      justify='left')
                        codigo_result.place(bordermode='outside', height=25, width=300, x=350, y=80)
                        cliente_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.cliente,
                                                       justify='left')
                        cliente_result.place(bordermode='outside', height=25, width=300, x=350, y=105)
                        estado_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.estado_servicio,
                                                      justify='left')
                        estado_result.place(bordermode='outside', height=25, width=300, x=350, y=130)
                        fecha_entrega_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.fecha_entrega,
                                                             justify='left')
                        fecha_entrega_result.place(bordermode='outside', height=25, width=300, x=350, y=155)
                        fecha_retiro_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.fecha_retiro,
                                                            justify='left')
                        fecha_retiro_result.place(bordermode='outside', height=25, width=300, x=350, y=180)
                        maquina_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.maquina['codigom'],
                                                       justify='left')
                        maquina_result.place(bordermode='outside', height=25, width=300, x=350, y=205)
                        barril_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.barril['codigob'],
                                                      justify='left')
                        barril_result.place(bordermode='outside', height=25, width=300, x=350, y=230)
                        cantidad_result = tkinter.Label(finalizarVentana, font='Arial', text=dato.cantidad_barril,
                                                        justify='left')
                        cantidad_result.place(bordermode='outside', height=25, width=300, x=350, y=255)

                        cantidad_result_consig = tkinter.Label(finalizarVentana, font='Arial', text=0,
                                                            justify='left')
                        utilizoConsig = ['SI', 'NO']
                        dato.consig_ejecutado = 'NO'
                        utilizo_consignacion = ttk.Combobox(finalizarVentana, values=utilizoConsig, state='readonly')
                        if dato.tipo == 'CONSIGNACION':
                            #estado_result_consig = tkinter.Label(finalizarVentana, font='Arial', text=dato.estado_consignacion, justify='left')
                            #estado_result_consig.place(bordermode='outside', height=25, width=300, x=350, y=305)
                            cantidad_result_consig.config(text=dato.cantidad_consignacion)
                            cantidad_result_consig.place(bordermode='outside', height=25, width=300, x=350, y=280)

                            utilizo_consignacion.place(bordermode='outside', height=25, width=300, x=350, y=305)
                            #asocio el valor no por defecto
                            utilizo_consignacion.current(1)
                            #asocio al cambio del select el calculo del total
                            utilizo_consignacion.bind("<<ComboboxSelected>>", actualizar_costo_result)
                        else:
                            cantidad_result_consig.config(text=0)
                            cantidad_result_consig.place(bordermode='outside', height=25, width=300, x=350, y=280)
                            utilizo_consignacion.config(values=['-'])
                            utilizo_consignacion.place(bordermode='outside', height=25, width=300, x=350, y=305)
                            utilizo_consignacion.current(0)
                        costo_result = tkinter.Label(finalizarVentana, font='Arial', text=f"{dato.costo_total:,.0f}".replace(",", "."),
                                                     justify='left')
                        costo_result.place(bordermode='outside', height=25, width=300, x=350, y=360)

                        finalizar_btn = tkinter.Button(finalizarVentana, text="Finalizar",
                                                       command=finalizar_operacion)
                        finalizar_btn.place(bordermode='outside', height=40, width=100, x=240, y=420)

                        #volver = tkinter.Button(finalizarVentana, text="Volver", command=cerrar_exp)
                        #volver.place(bordermode='outside', height=40, width=100, x=500, y=330)
                    else:
                        alerta = tkinter.Message(finalizarVentana, relief='raised', text='No se encontraron registros',
                                                 width=200)
                        alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                        ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                        ok.pack(side="bottom")
            except Exception as e:
                alerta = tkinter.Message(finalizarVentana, relief='raised',
                                         text='NO SE PUDO FINALIZAR EL SERVICIO\nError: \n' + str(e), width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                ok.pack(side="bottom")




        titulo = tkinter.Label(finalizarVentana, font='Arial', text="DATOS DEL SERVICIO AGENDADO")
        titulo.place(bordermode='outside', height=25, width=300, x=100)

        # Etiquetas
        lbl_codigo = tkinter.Label(finalizarVentana, font='Arial', text="Código del Servicio", justify='left')
        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=30)

        # Campos de Texto
        codigo = tkinter.Entry(finalizarVentana, font='times')
        codigo.place(bordermode='outside', height=25, width=300, x=350, y=30)

        buscar = tkinter.Button(finalizarVentana, text="Buscar", command=buscar)
        buscar.place(bordermode='outside', height=40, width=100, x=40, y=420)
        volver = tkinter.Button(finalizarVentana, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=420)
        finalizarVentana.mainloop()
