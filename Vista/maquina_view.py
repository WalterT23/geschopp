'''Vista para la carga de maquinas'''
from Util.util import Util
from Modelo.maquina import Maquina
from Controlador.maquina_controller import ControladorMaquina
import tkinter


class MaquinaView:
    def __init__(self):
        self.util = Util()
        self.controller = ControladorMaquina()
    def cargar_maquina(self):
        maquinas = tkinter.Tk()
        maquinas.title("CARGAR MAQUINA")
        maquinas.geometry("800x500")

        def volver():
            maquinas.destroy()

        def cargar():
            def cerrar_exp():
                maquinas.destroy()
                self.cargar_maquina()

            def cerrar_alerta():
                alerta.destroy()

            try:
                nro_maquina_ = self.util.validar_entero(str(nro_maquina.get()), 1, "Nro Maquina:")
                peso_ = self.util.validar_entero(str(peso.get()), 1, "Peso:")
                estado_ = self.util.validar_cadena(str(estado.get()), True, "Estado:")
                cantidad_ = self.util.validar_entero(str(cantidad.get()), 1, "Cantidad:")

                contenedor = Maquina(nro_maquina_, peso_, estado_, cantidad_)

                codigoMaquina = self.controller.cargar_maquina(contenedor)

            except Exception as e:
                alerta = tkinter.Message(maquinas, relief='raised',
                                         text='NO SE PUDO CARGAR LA MAQUINA\nError: \n' + str(e), width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                ok.pack(side="bottom")
            else:
                alerta = tkinter.Message(maquinas, relief='raised', text='MAQUINA CARGADA CON EXITO', width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")

        titulo = tkinter.Label(maquinas, font='Arial', text="DATOS DE LA NUEVA MAQUINA")
        titulo.place(bordermode='outside', height=25, width=350, x=100)
        # Etiquetas
        lbl_nro_maquina = tkinter.Label(maquinas, font='Arial', text="Nro. Maquina", justify='left')
        lbl_nro_maquina.place(bordermode='outside', height=25, width=200, x=50, y=30)
        lbl_peso = tkinter.Label(maquinas, font='Arial', text="Peso")
        lbl_peso.place(bordermode='outside', height=25, width=200, x=50, y=55)
        lbl_estado = tkinter.Label(maquinas, font='Arial', text="Estado")
        lbl_estado.place(bordermode='outside', height=25, width=200, x=50, y=80)
        lbl_cantidad = tkinter.Label(maquinas, font='Arial', text="Cantidad")
        lbl_cantidad.place(bordermode='outside', height=25, width=200, x=50, y=105)

        # Campos de Texto
        nro_maquina = tkinter.Entry(maquinas, font='times')
        nro_maquina.place(bordermode='outside', height=25, width=200, x=250, y=30)
        peso = tkinter.Entry(maquinas, font='times')
        peso.place(bordermode='outside', height=25, width=200, x=250, y=55)
        estado = tkinter.Entry(maquinas, font='times')
        estado.place(bordermode='outside', height=25, width=200, x=250, y=80)
        cantidad = tkinter.Entry(maquinas, font='times')
        cantidad.place(bordermode='outside', height=25, width=200, x=250, y=105)

        cargar = tkinter.Button(maquinas, text="Cargar", command=cargar)
        cargar.place(bordermode='outside', height=40, width=100, x=40, y=135)
        volver = tkinter.Button(maquinas, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=135)
        maquinas.mainloop()

    def solicitar_codigo(self):
        maquinas = tkinter.Tk()
        maquinas.title("BUSCAR MAQUINA")
        maquinas.geometry("800x500")

        def volver():
            maquinas.destroy()

        def buscar():
            def cerrar_exp():
                maquinas.destroy()
                self.solicitar_codigo()

            dato = self.controller.buscar_maquina(self.util.validar_entero(str(codigo.get()), 1))
            if dato != None and len(dato) > 0:
                lbl_codigo = tkinter.Label(maquinas, font='Arial', text="Código", justify='left')
                lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=55)
                lbl_nro_maquina = tkinter.Label(maquinas, font='Arial', text="Nro. Maquina", justify='left')
                lbl_nro_maquina.place(bordermode='outside', height=25, width=300, x=50, y=80)
                lbl_peso = tkinter.Label(maquinas, font='Arial', text="Peso", justify='left')
                lbl_peso.place(bordermode='outside', height=25, width=300, x=50, y=105)
                lbl_cantidad = tkinter.Label(maquinas, font='Arial', text="Cantidad", justify='left')
                lbl_cantidad.place(bordermode='outside', height=25, width=300, x=50, y=130)
                lbl_estado = tkinter.Label(maquinas, font='Arial', text="Estado", justify='left')
                lbl_estado.place(bordermode='outside', height=25, width=300, x=50, y=155)

                codigo_result = tkinter.Label(maquinas, font='Arial', text=dato['codigom'], justify='left')
                codigo_result.place(bordermode='outside', height=25, width=300, x=350, y=55)
                nro_maquina_result = tkinter.Label(maquinas, font='Arial', text=dato['nro_maquina'], justify='left')
                nro_maquina_result.place(bordermode='outside', height=25, width=300, x=350, y=80)
                peso_result = tkinter.Label(maquinas, font='Arial', text=dato['peso'], justify='left')
                peso_result.place(bordermode='outside', height=25, width=300, x=350, y=105)
                cantidad_result = tkinter.Label(maquinas, font='Arial', text=dato['cantidad'], justify='left')
                cantidad_result.place(bordermode='outside', height=25, width=300, x=350, y=130)
                estado_result = tkinter.Label(maquinas, font='Arial', text=dato['estado'], justify='left')
                estado_result.place(bordermode='outside', height=25, width=300, x=350, y=155)
            else:

                alerta = tkinter.Message(maquinas, relief='raised', text='Maquina no encontrada', width=200)
                alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")



        titulo = tkinter.Label(maquinas, font='Arial', text="DATOS MAQUINA")
        titulo.place(bordermode='outside', height=25, width=300, x=100)

        # Etiquetas
        lbl_codigo = tkinter.Label(maquinas, font='Arial', text="Código de la Maquina", justify='left')
        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=30)

        # Campos de Texto
        codigo = tkinter.Entry(maquinas, font='times')
        codigo.place(bordermode='outside', height=25, width=300, x=350, y=30)

        buscar = tkinter.Button(maquinas, text="Buscar", command=buscar)
        buscar.place(bordermode='outside', height=40, width=100, x=40, y=210)
        volver = tkinter.Button(maquinas, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=210)
        maquinas.mainloop()

    def listar_maquinas(self):
        def cerrar_exp():
            maquinas.destroy()
            self.cargar_maquina()

        maquinas = tkinter.Tk()
        maquinas.title("LISTADO DE MAQUINAS")
        maquinas.geometry("1000x500")

        def volver():
            maquinas.destroy()

        mylistbox = tkinter.Listbox(maquinas, height=20, width=100, font=('times', 13))
        mylistbox.place(x=32, y=110)
        fun = self.controller.listar_maquina()
        if fun != None and len(fun) != 0:
            for values in fun:
                mylistbox.insert('end', '* Codigo: ' + str(values.codigom) + ', Peso: ' + str(values.peso)+ ', Estado: ' + values.estado +', Cantidad: ' + str(values.cantidad))
            titulo = tkinter.Label(maquinas, font='Arial', text="LISTADO DE MAQUINAS")
            titulo.place(bordermode='outside', height=25, width=600, x=100, y=30)
            volver = tkinter.Button(maquinas, text="Volver", command=volver)
            volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
            maquinas.mainloop()
        else:
            alerta = tkinter.Message(maquinas, relief='raised',
                                     text='NO EXISTEN REGISTROS ', width=200)
            alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
            ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
            ok.pack(side="bottom")




