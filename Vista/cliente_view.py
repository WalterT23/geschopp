'''Vista para la carga de paciente'''
from Util.util import Util
from Modelo.cliente_model import Cliente
from Controlador.cliente_controller import ControladorCliente
import tkinter

class ClienteView:
    def __init__(self):
        self.util = Util()
        self.controller = ControladorCliente()

    def cargar_cliente(self):
        clientes = tkinter.Tk()
        clientes.title("CARGAR CLIENTE")
        clientes.geometry("800x500")

        def volver():
            clientes.destroy()

        def cargar():
            def cerrar_exp():
                clientes.destroy()
                self.cargar_cliente()

            def cerrar_alerta():
                alerta.destroy()

            try:
                nombre_general = self.util.validar_cadena(str(nombre.get()), True, "Nombre:")
                apellido_general = self.util.validar_cadena(str(apellido.get()), True, "Apellido:")
                cedula_general = self.util.validar_entero(str(documento_identidad.get()), 1, "Cedula:")
                telefono_general = self.util.validar_cadena(str(telefono.get()), False, "Telefono:")
                email_general = self.util.validar_cadena(str(email.get()), False, "Correo:")
                fecha_general = self.util.validar_fecha(str(fecha_nacimiento.get()), "Fecha Nacimiento:")
                direccion_general = self.util.validar_cadena(str(direccion.get()), False, "Direccion:")

                contenedor = Cliente(nombre_general, apellido_general, cedula_general, fecha_general, telefono_general, email_general, direccion_general)
                codigo = self.controller.cargar_cliente(contenedor)

            except Exception as e:
                alerta = tkinter.Message(clientes, relief='raised',
                                         text='NO SE PUDO CARGAR EL PACIENTE\nError: ' + str(e), width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                ok.pack(side="bottom")
            else:
                alerta = tkinter.Message(clientes, relief='raised', text='CLIENTE '+codigo+' CARGADO CON EXITO', width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")

        titulo = tkinter.Label(clientes, font='Arial', text="DATOS DEL NUEVO CLIENTE")
        titulo.place(bordermode='outside', height=25, width=350, x=100)
        # Etiquetas
        lbl_nombre = tkinter.Label(clientes, font='Arial', text="Nombres", justify='left')
        lbl_nombre.place(bordermode='outside', height=25, width=200, x=50, y=30)
        lbl_apellido = tkinter.Label(clientes, font='Arial', text="Apellidos")
        lbl_apellido.place(bordermode='outside', height=25, width=200, x=50, y=55)
        lbl_documento_identidad = tkinter.Label(clientes, font='Arial', text="Documento de identidad")
        lbl_documento_identidad.place(bordermode='outside', height=25, width=200, x=50, y=80)
        lbl_telefono = tkinter.Label(clientes, font='Arial', text="Télefono")
        lbl_telefono.place(bordermode='outside', height=25, width=200, x=50, y=105)
        lbl_email = tkinter.Label(clientes, font='Arial', text="Email")
        lbl_email.place(bordermode='outside', height=25, width=200, x=50, y=130)
        lbl_direccion = tkinter.Label(clientes, font='Arial', text="Direccion")
        lbl_direccion.place(bordermode='outside', height=25, width=200, x=50, y=155)
        lbl_fecha_nacimiento = tkinter.Label(clientes, font='Arial', text="Fecha de Nacimiento")
        lbl_fecha_nacimiento.place(bordermode='outside', height=25, width=200, x=50, y=180)

        # Campos de Texto
        nombre = tkinter.Entry(clientes, font='times')
        nombre.place(bordermode='outside', height=25, width=200, x=250, y=30)
        apellido = tkinter.Entry(clientes, font='times')
        apellido.place(bordermode='outside', height=25, width=200, x=250, y=55)
        documento_identidad = tkinter.Entry(clientes, font='times')
        documento_identidad.place(bordermode='outside', height=25, width=200, x=250, y=80)
        telefono = tkinter.Entry(clientes, font='times')
        telefono.place(bordermode='outside', height=25, width=200, x=250, y=105)
        email = tkinter.Entry(clientes, font='times')
        email.place(bordermode='outside', height=25, width=200, x=250, y=130)
        direccion = tkinter.Entry(clientes, font='times')
        direccion.place(bordermode='outside', height=25, width=200, x=250, y=155)
        fecha_nacimiento = tkinter.Entry(clientes, font='times')
        fecha_nacimiento.place(bordermode='outside', height=25, width=200, x=250, y=180)


        cargar = tkinter.Button(clientes, text="Cargar", command=cargar)
        cargar.place(bordermode='outside', height=40, width=100, x=240, y=260)
        volver = tkinter.Button(clientes, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=340, y=260)
        clientes.mainloop()


    def buscar_cliente(self):
        clientes = tkinter.Tk()
        clientes.title("BUSCAR CLIENTE")
        clientes.geometry("800x500")

        def volver():
            clientes.destroy()

        def buscar():

            def cerrar_exp():
                clientes.destroy()
                self.buscar_cliente()

            dato = self.controller.buscar_cliente(self.util.validar_cadena(str(codigo.get()), True))

            if dato != None and len(dato) > 0:

                lbl_codigo = tkinter.Label(clientes, font='Arial', text="Código", justify='left')
                lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=55)
                lbl_nombre = tkinter.Label(clientes, font='Arial', text="Nombre Completo", justify='left')
                lbl_nombre.place(bordermode='outside', height=25, width=300, x=50, y=80)
                lbl_documento_identidad = tkinter.Label(clientes, font='Arial', text="Documento de identidad", justify='left')
                lbl_documento_identidad.place(bordermode='outside', height=25, width=300, x=50, y=105)

                codigo_result = tkinter.Label(clientes, font='Arial', text=dato['codigo'], justify='left')
                codigo_result.place(bordermode='outside', height=25, width=300, x=350, y=55)
                nombre_result = tkinter.Label(clientes, font='Arial', text=dato['nombrecompleto'], justify='left')
                nombre_result.place(bordermode='outside', height=25, width=300, x=350, y=80)
                documento_identidad_result = tkinter.Label(clientes, font='Arial', text=dato['cedula'], justify='left')
                documento_identidad_result.place(bordermode='outside', height=25, width=300, x=350, y=105)
            else:

                alerta = tkinter.Message(clientes, relief='raised', text='Cliente no encontrado', width=200)
                alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")



        titulo = tkinter.Label(clientes, font='Arial', text="DATOS DEL CLIENTE")
        titulo.place(bordermode='outside', height=25, width=300, x=100)

        # Etiquetas
        lbl_codigo = tkinter.Label(clientes, font='Arial', text="Código del Cliente", justify='left')
        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=30)

        # Campos de Texto
        codigo = tkinter.Entry(clientes, font='times')
        codigo.place(bordermode='outside', height=25, width=300, x=350, y=30)

        buscar = tkinter.Button(clientes, text="Buscar", command=buscar)
        buscar.place(bordermode='outside', height=40, width=100, x=40, y=210)
        volver = tkinter.Button(clientes, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=210)
        clientes.mainloop()

    def listar_clientes(self):
        def cerrar_exp():
            clientes.destroy()
            self.cargar_cliente()

        clientes = tkinter.Tk()
        clientes.title("LISTADO DE CLIENTES")
        clientes.geometry("1000x500")

        def volver():
            clientes.destroy()

        mylistbox = tkinter.Listbox(clientes, height=20, width=100, font=('times', 13))
        mylistbox.place(x=32, y=110)
        clientes_list = self.controller.listar_clientes()
        if clientes_list != None and len(clientes_list) != 0:
            for values in clientes_list:
                mylistbox.insert('end', '* Codigo: ' + values.codigof + ', Nombre: ' + values.nombre+' '+values.apellido)
            titulo = tkinter.Label(clientes, font='Arial', text="LISTADO DE CLIENTES")
            titulo.place(bordermode='outside', height=25, width=600, x=100, y=30)
            volver = tkinter.Button(clientes, text="Volver", command=volver)
            volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
            clientes.mainloop()
        else:
            alerta = tkinter.Message(clientes, relief='raised',
                                     text='NO EXISTEN REGISTROS ' , width=200)
            alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
            ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
            ok.pack(side="bottom")

    def busqueda_cedula_cliente(self, orden):
        clientes = tkinter.Tk()
        clientes.title("BUSCAR CLIENTE")
        clientes.geometry("800x500")

        def volver():
            clientes.destroy()

        def buscar():
            def cerrar_exp():
                clientes.destroy()

            try:
                codigo = self.util.validar_entero(str(cedula.get()), 1)
                clientes.destroy()
                #orden.gestionar_orden(codigo)

            except Exception as e:
                alerta = tkinter.Message(clientes, relief='raised',
                                         text='NO SE PUDO REALIZAR LA BUSQUEDA\nError: ' + str(e), width=200)
                alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")

        titulo = tkinter.Label(clientes, font='Arial', text="DATOS DEL CLIENTE")
        titulo.place(bordermode='outside', height=25, width=300, x=100)

        # Etiquetas
        lbl_codigo = tkinter.Label(clientes, font='Arial', text="Nro. de Cédula", justify='left')
        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=30)

        # Campos de Texto
        cedula = tkinter.Entry(clientes, font='times')
        cedula.place(bordermode='outside', height=25, width=300, x=350, y=30)

        buscar = tkinter.Button(clientes, text="Buscar", command=buscar)
        buscar.place(bordermode='outside', height=40, width=100, x=40, y=210)
        volver = tkinter.Button(clientes, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=210)
        clientes.mainloop()

    """def registrar_paciente_orden(self,orden, cedula):
        clientes = tkinter.Tk()
        clientes.title("CARGAR PACIENTE")
        clientes.geometry("800x500")

        def volver():
            clientes.destroy()

        def cargar():
            def cerrar_exp():
                clientes.destroy()
                #clientes.eval('::ttk::CancelRepeat')
                self.cargar_paciente()
            def ok_exp():
                clientes.destroy()
                #clientes.eval('::ttk::CancelRepeat')
                orden.gestionar_orden(cedula)

            try:

                nombre_pac = self.util.validar_cadena(str(nombre.get()), True)
                apellido_pac = self.util.validar_cadena(str(apellido.get()), True)
                cedula_pac = self.util.validar_entero(cedula, 1)
                telefono_pac = self.util.validar_cadena(str(telefono.get()), False)
                email_pac = self.util.validar_cadena(str(email.get()), False)
                fecha_pac = self.util.validar_fecha(str(fecha_nacimiento.get()))

                contenedor = Cliente(nombre_pac, apellido_pac, cedula_pac, '')

                self.controller.cargar_paciente(contenedor)

            except Exception as e:
                alerta = tkinter.Message(clientes, relief='raised',
                                         text='NO SE PUDO CARGAR EL PACIENTE\nError: ' + str(e), width=200)
                alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")
            else:
                alerta = tkinter.Message(clientes, relief='raised', text='PACIENTE CARGADO CON EXITO', width=200)
                alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=ok_exp)
                ok.pack(side="bottom")

        titulo = tkinter.Label(clientes, font='Arial', text="DATOS DEL NUEVO PACIENTE")
        titulo.place(bordermode='outside', height=25, width=600, x=100)
        # Etiquetas
        lbl_nombre = tkinter.Label(clientes, font='Arial', text="Nombres", justify='left')
        lbl_nombre.place(bordermode='outside', height=25, width=300, x=50, y=30)
        lbl_apellido = tkinter.Label(clientes, font='Arial', text="Apellidos")
        lbl_apellido.place(bordermode='outside', height=25, width=300, x=50, y=55)
        lbl_documento_identidad = tkinter.Label(clientes, font='Arial', text="Documento de identidad")
        lbl_documento_identidad.place(bordermode='outside', height=25, width=300, x=50, y=80)
        lbl_telefono = tkinter.Label(clientes, font='Arial', text="Télefono")
        lbl_telefono.place(bordermode='outside', height=25, width=300, x=50, y=105)
        lbl_email = tkinter.Label(clientes, font='Arial', text="Email")
        lbl_email.place(bordermode='outside', height=25, width=300, x=50, y=130)
        lbl_fecha_nacimiento = tkinter.Label(clientes, font='Arial', text="Fecha de Nacimiento")
        lbl_fecha_nacimiento.place(bordermode='outside', height=25, width=300, x=50, y=155)

        # Campos de Texto
        nombre = tkinter.Entry(clientes, font='times')
        nombre.place(bordermode='outside', height=25, width=300, x=350, y=30)
        apellido = tkinter.Entry(clientes, font='times')
        apellido.place(bordermode='outside', height=25, width=300, x=350, y=55)

        documento_identidad_result = tkinter.Label(clientes, font='Arial', text=cedula)
        documento_identidad_result.place(bordermode='outside', height=25, width=300, x=350, y=80)
        #documento_identidad = tkinter.Entry(clientes, font='times')
        #documento_identidad.place(bordermode='outside', height=25, width=300, x=350, y=80)

        telefono = tkinter.Entry(clientes, font='times')
        telefono.place(bordermode='outside', height=25, width=300, x=350, y=105)
        email = tkinter.Entry(clientes, font='times')
        email.place(bordermode='outside', height=25, width=300, x=350, y=130)
        fecha_nacimiento = tkinter.Entry(clientes, font='times')
        fecha_nacimiento.place(bordermode='outside', height=25, width=300, x=350, y=155)


        cargar = tkinter.Button(clientes, text="Cargar", command=cargar)
        cargar.place(bordermode='outside', height=40, width=100, x=240, y=210)
        volver = tkinter.Button(clientes, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=340, y=210)
        clientes.mainloop()
"""




