'''Vista para la carga de empleados'''
from Util.util import Util
from Modelo.empleado import Empleado
from Controlador.empleado_controller import ControladorEmpleado
import tkinter



#from Vista.vistaLab import VistaLab

class EmpleadoView:
    def __init__(self):
        self.util = Util()
        self.controller = ControladorEmpleado()
    def cargar_empleado(self):
        empleados = tkinter.Tk()
        empleados.title("CARGAR EMPLEADO")
        empleados.geometry("800x500")

        def volver():
            empleados.destroy()



        def cargar():
            def cerrar_exp():
                empleados.destroy()
                self.cargar_empleado()

            def cerrar_alerta():
                alerta.destroy()

            try:

                nombre_emp = self.util.validar_cadena(str(nombre.get()), True, "Nombre:")
                apellido_emp = self.util.validar_cadena(str(apellido.get()), True, "Apellido:")
                cedula_emp = self.util.validar_entero(str(documento_identidad.get()), 1, "Cedula:")
                telefono_emp = self.util.validar_cadena(str(telefono.get()), False, "Telefono:")
                email_emp = self.util.validar_cadena(str(email.get()), False, "Correo:")
                fecha_emp = self.util.validar_fecha(str(fecha_nacimiento.get()),"Fecha Nacimiento:")
                cargo_emp = self.util.validar_cadena(str(cargo.get()), False, "Cargo:")
                sueldo_emp = self.util.validar_entero(str(sueldo.get()), True, "Sueldo:")
                direccion_emp = self.util.validar_cadena(str(direccion.get()), False, "Direccion:")

                contenedor = Empleado(nombre_emp, apellido_emp, cedula_emp, fecha_emp, telefono_emp, email_emp, direccion_emp, cargo_emp, sueldo_emp)

                codigoEmpleado = self.controller.cargar_func(contenedor)

            except Exception as e:
                alerta = tkinter.Message(empleados, relief='raised',
                                         text='NO SE PUDO CARGAR AL EMPLEADO\nError: \n' + str(e), width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_alerta)
                ok.pack(side="bottom")
            else:
                alerta = tkinter.Message(empleados, relief='raised', text='EMPLEADO '+codigoEmpleado+' CARGADO CON EXITO', width=300)
                alerta.place(bordermode='outside', height=250, width=350, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")

        titulo = tkinter.Label(empleados, font='Arial', text="DATOS DEL NUEVO EMPLEADO")
        titulo.place(bordermode='outside', height=25, width=350, x=100)
        # Etiquetas
        lbl_nombre = tkinter.Label(empleados, font='Arial', text="Nombres", justify='left')
        lbl_nombre.place(bordermode='outside', height=25, width=200, x=50, y=30)
        lbl_apellido = tkinter.Label(empleados, font='Arial', text="Apellidos")
        lbl_apellido.place(bordermode='outside', height=25, width=200, x=50, y=55)
        lbl_documento_identidad = tkinter.Label(empleados, font='Arial', text="Documento de identidad")
        lbl_documento_identidad.place(bordermode='outside', height=25, width=200, x=50, y=80)
        lbl_telefono = tkinter.Label(empleados, font='Arial', text="Télefono")
        lbl_telefono.place(bordermode='outside', height=25, width=200, x=50, y=105)
        lbl_email = tkinter.Label(empleados, font='Arial', text="Email")
        lbl_email.place(bordermode='outside', height=25, width=200, x=50, y=130)
        lbl_direccion = tkinter.Label(empleados, font='Arial', text="Direccion")
        lbl_direccion.place(bordermode='outside', height=25, width=200, x=50, y=155)
        lbl_fecha_nacimiento = tkinter.Label(empleados, font='Arial', text="Fecha de Nacimiento")
        lbl_fecha_nacimiento.place(bordermode='outside', height=25, width=200, x=50, y=180)
        lbl_cargo = tkinter.Label(empleados, font='Arial', text="Cargo")
        lbl_cargo.place(bordermode='outside', height=25, width=200, x=50, y=205)
        lbl_sueldo = tkinter.Label(empleados, font='Arial', text="Sueldo")
        lbl_sueldo.place(bordermode='outside', height=25, width=200, x=50, y=230)

        # Campos de Texto
        nombre = tkinter.Entry(empleados, font='times')
        nombre.place(bordermode='outside', height=25, width=200, x=250, y=30)
        apellido = tkinter.Entry(empleados, font='times')
        apellido.place(bordermode='outside', height=25, width=200, x=250, y=55)
        documento_identidad = tkinter.Entry(empleados, font='times')
        documento_identidad.place(bordermode='outside', height=25, width=200, x=250, y=80)
        telefono = tkinter.Entry(empleados, font='times')
        telefono.place(bordermode='outside', height=25, width=200, x=250, y=105)
        email = tkinter.Entry(empleados, font='times')
        email.place(bordermode='outside', height=25, width=200, x=250, y=130)
        direccion = tkinter.Entry(empleados, font='times')
        direccion.place(bordermode='outside', height=25, width=200, x=250, y=155)
        fecha_nacimiento = tkinter.Entry(empleados, font='times')
        fecha_nacimiento.place(bordermode='outside', height=25, width=200, x=250, y=180)
        cargo = tkinter.Entry(empleados, font='times')
        cargo.place(bordermode='outside', height=25, width=200, x=250, y=205)
        sueldo = tkinter.Entry(empleados, font='times')
        sueldo.place(bordermode='outside', height=25, width=200, x=250, y=230)


        cargar = tkinter.Button(empleados, text="Cargar", command=cargar)
        cargar.place(bordermode='outside', height=40, width=100, x=40, y=260)
        volver = tkinter.Button(empleados, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=260)
        empleados.mainloop()

    def solicitar_codigo(self):
        empleados = tkinter.Tk()
        empleados.title("BUSCAR EMPLEADO")
        empleados.geometry("800x500")

        def volver():
            empleados.destroy()

        def buscar():
            def cerrar_exp():
                empleados.destroy()
                self.solicitar_codigo()

            dato = self.controller.buscar_empleado(self.util.validar_cadena(str(codigo.get()), True))
            if dato != None and len(dato) > 0:
                lbl_codigo = tkinter.Label(empleados, font='Arial', text="Código", justify='left')
                lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=55)
                lbl_nombre = tkinter.Label(empleados, font='Arial', text="Nombre Completo", justify='left')
                lbl_nombre.place(bordermode='outside', height=25, width=300, x=50, y=80)
                lbl_documento_identidad = tkinter.Label(empleados, font='Arial', text="Documento de identidad", justify='left')
                lbl_documento_identidad.place(bordermode='outside', height=25, width=300, x=50, y=105)
                lbl_cargo = tkinter.Label(empleados, font='Arial', text="Cargo", justify='left')
                lbl_cargo.place(bordermode='outside', height=25, width=300, x=50, y=130)

                codigo_result = tkinter.Label(empleados, font='Arial', text=dato['codigo'], justify='left')
                codigo_result.place(bordermode='outside', height=25, width=300, x=350, y=55)
                nombre_result = tkinter.Label(empleados, font='Arial', text=dato['nombrecompleto'], justify='left')
                nombre_result.place(bordermode='outside', height=25, width=300, x=350, y=80)
                documento_identidad_result = tkinter.Label(empleados, font='Arial', text=dato['cedula'], justify='left')
                documento_identidad_result.place(bordermode='outside', height=25, width=300, x=350, y=105)
                cargo_result = tkinter.Label(empleados, font='Arial', text=dato['cargo'], justify='left')
                cargo_result.place(bordermode='outside', height=25, width=300, x=350, y=130)
            else:

                alerta = tkinter.Message(empleados, relief='raised', text='Empleado no encontrado', width=200)
                alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
                ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
                ok.pack(side="bottom")



        titulo = tkinter.Label(empleados, font='Arial', text="DATOS EMPLEADO")
        titulo.place(bordermode='outside', height=25, width=300, x=100)

        # Etiquetas
        lbl_codigo = tkinter.Label(empleados, font='Arial', text="Código del Empleado", justify='left')
        lbl_codigo.place(bordermode='outside', height=25, width=300, x=50, y=30)

        # Campos de Texto
        codigo = tkinter.Entry(empleados, font='times')
        codigo.place(bordermode='outside', height=25, width=300, x=350, y=30)

        buscar = tkinter.Button(empleados, text="Buscar", command=buscar)
        buscar.place(bordermode='outside', height=40, width=100, x=40, y=210)
        volver = tkinter.Button(empleados, text="Volver", command=volver)
        volver.place(bordermode='outside', height=40, width=100, x=140, y=210)
        empleados.mainloop()

    def listar_empleados(self):
        def cerrar_exp():
            empleados.destroy()
            self.cargar_empleado()

        empleados = tkinter.Tk()
        empleados.title("LISTADO DE EMPLEADOS")
        empleados.geometry("1000x500")

        def volver():
            empleados.destroy()

        mylistbox = tkinter.Listbox(empleados, height=20, width=100, font=('times', 13))
        mylistbox.place(x=32, y=110)
        fun = self.controller.listar_empleado()
        if fun != None and len(fun) != 0:
            for values in fun:
                mylistbox.insert('end', '* Codigo: ' + values.codigof + ', Nombre: ' + values.nombre+' '+values.apellido + ', Cargo: '+values.cargo)
            titulo = tkinter.Label(empleados, font='Arial', text="LISTADO DE EMPLEADOS")
            titulo.place(bordermode='outside', height=25, width=600, x=100, y=30)
            volver = tkinter.Button(empleados, text="Volver", command=volver)
            volver.place(bordermode='outside', height=40, width=100, x=40, y=400)
            empleados.mainloop()
        else:
            alerta = tkinter.Message(empleados, relief='raised',
                                     text='NO EXISTEN REGISTROS ', width=200)
            alerta.place(bordermode='outside', height=150, width=200, y=30, x=150)
            ok = tkinter.Button(alerta, text="Ok", command=cerrar_exp)
            ok.pack(side="bottom")

    def mostrar_resultado(self,dato):
        if dato != None and len(dato) > 0:
            print('\n-> Empleado Buscado Codigo: ', dato['codigo'])
            print('* Nombre Completo: ', dato['nombrecompleto'])
            print('* Cedula: ', dato['cedula'])
            print('* Cargo: ', dato['cargo']+'\n')
        else:
            print('\n-> Empleado no encontrado')
        self.util.pause()

    def mostrar_msg(self,msg):
        print('\n',msg+'\n\n')




