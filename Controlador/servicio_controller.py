from Controlador.barril_controller import ControladorBarril
from Controlador.maquina_controller import ControladorMaquina
from Modelo.servicio_consignacion import ServicioConsignacion

from Modelo.servicio_normal import ServicioNormal


class ServicioController:

    def __init__(self):
        self.ctrl_barril = ControladorBarril()
        self.ctrl_maquina = ControladorMaquina()
        self.modelServicio = ServicioNormal('','','','','')
        self.modelServicioConsig = ServicioConsignacion('', '', '', '', '')


    ''' servicio.cliente = cedula, servicio.barril = codigoBarril, servicio.maquina = codigoMaquina
        reemplazar el valor del codigo por el objeto
    '''
    def cargar_agendamiento(self, servicio):
        servicio.idservicio = servicio.generar_id_servicio()
        servicio.idservicio = servicio.idservicio + 1
        servicio.barril = self.ctrl_barril.buscar_barril(servicio.barril)
        servicio.maquina = self.ctrl_maquina.buscar_maquina(servicio.maquina)
        servicio.cargar_servicio(servicio, servicio.idservicio)
        return servicio.idservicio

    def listar_agendamientos(self):
        lista_aux = self.modelServicio.listar_servicio()
        lista_aux.extend(self.modelServicioConsig.listar_servicio())
        return lista_aux

    '''Devuelve un objeto que contiene todos los servicios'''
    def buscar_servicio_codigo(self, codigo):
        objeto = self.modelServicio.buscar_servicio_cod(codigo)
        if objeto == {}:
            objeto = self.modelServicioConsig.buscar_servicio_cod(codigo)
        return objeto

    '''Devuelve una lista que contiene todos los servicios'''
    def buscar_servicio_cedula(self, cedula):
        listax = self.modelServicio.buscar_servicio_ced(cedula)
        listax.extend(self.modelServicioConsig.buscar_servicio_ced(cedula))
        return listax

    def calcular_servicio(self, codigo):
        objetoc = self.modelServicio.calcular_costo(codigo)
        if objetoc == {}:
            objetoc = self.modelServicioConsig.calcular_costo(codigo)
        return objetoc

    def finalizar_ope(self, objeto, codigo):
        return objeto.finalizar_servicio(objeto, codigo)

    def eliminar_todo(self):
        self.modelServicio.borrar_servicios_normales()
        self.modelServicioConsig.borrar_servicios_consignacion()






