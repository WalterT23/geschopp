from Modelo.empleado import Empleado
from Util.util import Util


class ControladorEmpleado():
    """Clase Controlador de los Empleados"""

    def __init__(self):
        self.fun = Empleado('', '', '', '', '', '', '', '', '')
        self.util = Util()
    def cargar_func(self, empleado):
        try:
            self.fun = empleado
            codigo = self.util.genera_codigo(self.fun.nombre, self.fun.apellido, self.fun.cedula)
            existe = self.fun.buscar_persona(codigo)
            if existe != None and len(existe) != 0:
                raise Exception('El empleado {} ya existe en la base'.format(codigo))

            else:
                self.fun.carga_datos(codigo)
                return self.fun.cargar_persona(self.fun, codigo)

        except KeyboardInterrupt as e:
            raise Exception('Carga interrumpida.')
        except Exception as ex:
            raise Exception(ex)

    def listar_empleado(self):
        return self.fun.listar_persona()

    def buscar_empleado(self,codigo ):
        return self.fun.buscar_persona(codigo)

