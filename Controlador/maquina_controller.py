from Modelo.maquina import Maquina
from Util.util import Util


class ControladorMaquina:
    """Clase Controlador de las Maquinas"""

    def __init__(self):
        self.fun = Maquina('', '', '', '')
        self.util = Util()

    def cargar_maquina(self, maquina):
        try:
            self.fun = maquina
            codigo = maquina.nro_maquina
            existe = self.fun.buscar_maquina(codigo)
            if existe != None and len(existe) != 0:
                raise Exception('La maquina {} ya existe en la base'.format(codigo))

            else:
                self.fun.carga_datos(codigo)
                return self.fun.cargar_maquina(self.fun, codigo)

        except KeyboardInterrupt as e:
            raise Exception('Carga interrumpida.')
        except Exception as ex:
            raise Exception(ex)

    def listar_maquina(self):
        return self.fun.listar_maquina()

    def buscar_maquina(self, codigo):
        return self.fun.buscar_maquina(codigo)

