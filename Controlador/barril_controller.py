from Modelo.barril import Barril
from Util.util import Util


class ControladorBarril:
    """Clase Controlador de los Barriles"""

    def __init__(self):
        self.fun = Barril('', '', '')
        self.util = Util()
    def cargar_barril(self, barril):
        try:
            self.fun = barril
            codigo = self.util.genera_codigo(self.fun.marca, self.fun.marca, self.fun.capacidad)
            existe = self.fun.buscar_barril(codigo)
            if existe != None and len(existe) != 0:
                raise Exception('El barril {} ya existe en la base'.format(codigo))

            else:
                self.fun.carga_datos(codigo)
                return self.fun.cargar_barril(self.fun, codigo)

        except KeyboardInterrupt as e:
            raise Exception('Carga interrumpida.')
        except Exception as ex:
            raise Exception(ex)

    def listar_barril(self):
        return self.fun.listar_barril()

    def buscar_barril(self, codigo):
        return self.fun.buscar_barril(codigo)

