from Modelo.cliente_model import Cliente
from Util.util import Util


class ControladorCliente:
    """Clase Controlador de los Clientes"""

    def __init__(self):
        self.var = Cliente('', '', '', '', '', '', '')
        self.util = Util()

    def cargar_cliente(self, cliente):
        self.var = cliente
        codigo = self.util.genera_codigo(self.var.nombre, self.var.apellido, self.var.cedula)
        existe = self.var.buscar_persona(codigo)
        if existe != None and len(existe) != 0:
            raise Exception('El cliente {} ya esta existe en la base'.format(codigo))
        else:
            self.var.carga_datos(codigo)
            return self.var.cargar_persona(self.var, codigo)

    def listar_clientes(self):
        return self.var.listar_persona()

    def buscar_cliente(self, codigo):
        return self.var.buscar_persona(codigo)
