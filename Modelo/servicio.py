from abc import ABCMeta, abstractmethod


class Servicio(metaclass=ABCMeta):
    def __init__(self, cliente, fecha_entrega, fecha_retiro, maquina, barril):
        self.cliente = cliente
        self.fecha_entrega = fecha_entrega
        self.fecha_retiro = fecha_retiro
        self.maquina = maquina
        self.barril = barril
        self.cantidad_barril = 0
        self.estado_servicio = 'PENDIENTE'
        self.costo_total = 0
        self.idservicio = 0


    @abstractmethod
    def generar_id_servicio(self):
        pass

    @abstractmethod
    def cargar_servicio(self, objeto, idservicio):
        pass

    @abstractmethod
    def listar_servicio(self):
        pass

    @abstractmethod
    def buscar_servicio(self, idservicio):
        pass

    @abstractmethod
    def finalizar_servicio(self, objeto, idservicio):
        pass

    @abstractmethod
    def calcular_costo(self, idservicio):
        pass
