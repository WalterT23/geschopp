from Modelo.persona import Persona
from Util.base import MiZODB, transaction
from persistent import Persistent

class Empleado(Persona, Persistent):
    def __init__(self, nombre, apellido, cedula, fecha_nac, nro_celular, correo, direccion, cargo, sueldo):
        Persona.__init__(self, nombre, apellido, cedula, fecha_nac, nro_celular, correo, direccion)
        self.codigof = ''
        self.cargo = cargo
        self.sueldo = sueldo

    def carga_datos(self, codigo):
        self.codigof = codigo

    def cargar_persona(self, obj, clave):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            dbroot[clave] = obj
            transaction.commit()
            db.close()
            #return ('Empleado creado!: ' + clave)
            return clave
        except Exception as e:
            db.close()
            raise ('No se ha podido crear \n', e)

    def listar_persona(self):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Empleado):
                    empleado = Empleado(obj.nombre, obj.apellido, obj.cedula, obj.fecha_nac, '', '', '', obj.cargo, obj.sueldo)
                    empleado.codigof = obj.codigof
                    empleado.contacto = obj.contacto
                    result.append(empleado)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise('Error al consultar la base', e)

    def buscar_persona(self, codigo):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            persona = ''
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Empleado):
                    if obj.codigof == codigo:
                        persona = {'nombrecompleto': obj.nombre +
                                                   ' ' + obj.apellido, 'codigo': obj.codigof,'cedula':obj.cedula,'cargo':obj.cargo}
            db.close()
            return persona
        except Exception as e:
            db.close()


    def agregar_contacto(self, contacto):
        self.contacto.append(contacto)

    def obtener_contacto(self, nombre=None):
        for contacto in self.contacto:
            if nombre in contacto:
                print(f"Contacto encontrado: {contacto}")
                return


