from Modelo.servicio import Servicio
from Modelo.servicio_normal import ServicioNormal
from Util.base import MiZODB, transaction


class ServicioConsignacion(Servicio):

    def __init__(self, cliente, fecha_entrega, fecha_retiro, maquina, barril):
        Servicio.__init__(self, cliente, fecha_entrega, fecha_retiro, maquina, barril)
        self.cantidad_consignacion = 0
        self.estado_consignacion = 'PENDIENTE'
        self.tipo = 'CONSIGNACION'
        self.consig_ejecutado = 'NO'

    def borrar_servicios_consignacion(self):
        """
        Borra todos los registros de ServicioNormal en la base de datos.
        """
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            servicios_consignacion = [obj for obj in dbroot.values() if isinstance(obj, ServicioConsignacion)]

            for servicio in servicios_consignacion:
                del dbroot[servicio.idservicio]  # Elimina el registro

            transaction.commit()
            db.close()
        except Exception as e:
            db.close()
            raise RuntimeError(f"Error al borrar los datos de ServicioNormal: {e}")

    def generar_id_servicio(self):
        """
        Devuelve el mayor número de orden (ID de servicio) en la base de datos.

        Returns:
            int: El ID de servicio más alto encontrado.
        """
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            ids_servicio = [obj.idservicio for obj in dbroot.values() if isinstance(obj, Servicio)]
            db.close()
            return max(ids_servicio, default=0)
        except Exception as e:
            db.close()
            raise RuntimeError(f"Error al generar el ID de servicio: {e}")


    def cargar_servicio(self, objeto, idservicio):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            dbroot[idservicio] = objeto
            transaction.commit()
            db.close()
            return idservicio
        except Exception as e:
            db.close()
            raise ('No se ha podido crear el servicio \n', e)

    def listar_servicio(self):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, ServicioConsignacion):
                    servicio_consignacion = ServicioConsignacion(obj.cliente, obj.fecha_entrega, obj.fecha_retiro, obj.maquina, obj.barril)
                    servicio_consignacion.idservicio = obj.idservicio
                    servicio_consignacion.estado_servicio = obj.estado_servicio
                    servicio_consignacion.cantidad_barril = obj.cantidad_barril
                    servicio_consignacion.cantidad_consignacion = obj.cantidad_consignacion
                    servicio_consignacion.estado_consignacion = obj.estado_consignacion
                    result.append(servicio_consignacion)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise('Error al consultar la base', e)

    def buscar_servicio(self, idservicio):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = {}
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, ServicioConsignacion):
                    if (obj.idservicio == idservicio):
                        servicio_consignacion = ServicioConsignacion(obj.cliente, obj.fecha_entrega, obj.fecha_retiro,
                                                                     obj.maquina, obj.barril)
                        servicio_consignacion.idservicio = obj.idservicio
                        servicio_consignacion.estado_servicio = obj.estado_servicio
                        servicio_consignacion.cantidad_barril = obj.cantidad_barril
                        servicio_consignacion.cantidad_consignacion = obj.cantidad_consignacion
                        servicio_consignacion.costo_total = obj.costo_total
                        servicio_consignacion.estado_consignacion = obj.estado_consignacion
                        result = servicio_consignacion

            db.close()
            return result
        except Exception as e:
            db.close()
            raise ('Error al consultar la base', e)

    def buscar_servicio_cod(self, idservicio):
        return self.buscar_servicio(idservicio)

    def buscar_servicio_ced(self, cliente):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, ServicioConsignacion):
                    if obj.cliente == cliente:
                        servicio_consignacion = ServicioConsignacion(obj.cliente, obj.fecha_entrega, obj.fecha_retiro, obj.maquina, obj.barril)
                        servicio_consignacion.idservicio = obj.idservicio
                        servicio_consignacion.estado_servicio = obj.estado_servicio
                        servicio_consignacion.cantidad_barril = obj.cantidad_barril
                        servicio_consignacion.cantidad_consignacion = obj.cantidad_consignacion
                        servicio_consignacion.estado_consignacion = obj.estado_consignacion
                        result.append(servicio_consignacion)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise ('Error al consultar la base', e)

    def finalizar_servicio(self, objeto, idservicio):
        objeto.estado_servicio = 'FINALIZADA'
        if objeto.consig_ejecutado == 'NO':
            objeto.estado_consignacion = 'NO EJECUTADA'
        else:
            objeto.estado_consignacion = 'EJECUTADA'
        return self.cargar_servicio(objeto, idservicio)

    def calcular_costo(self, idservicio):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = {}
            obj = dbroot[idservicio]
            if isinstance(obj, ServicioConsignacion):
                if obj.estado_servicio == 'PENDIENTE':
                    servicio_consignacion = ServicioConsignacion(obj.cliente, obj.fecha_entrega, obj.fecha_retiro, obj.maquina, obj.barril)
                    servicio_consignacion.idservicio = obj.idservicio
                    servicio_consignacion.estado_servicio = obj.estado_servicio
                    servicio_consignacion.cantidad_barril = obj.cantidad_barril
                    servicio_consignacion.cantidad_consignacion = obj.cantidad_consignacion
                    servicio_consignacion.estado_consignacion = obj.estado_consignacion
                    if obj.estado_consignacion == 'CONSUMIDO':
                        servicio_consignacion.costo_total = (obj.cantidad_barril + obj.cantidad_consignacion) * obj.barril['precio']
                    else:
                        servicio_consignacion.costo_total = obj.barril['precio'] * obj.cantidad_barril
                    result = servicio_consignacion
            db.close()
            return result
        except Exception as e:
            db.close()
            raise ('Error al consultar la base', e)
