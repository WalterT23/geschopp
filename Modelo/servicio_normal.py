from Modelo.servicio import Servicio
from Util.base import MiZODB, transaction


class ServicioNormal(Servicio):

    def __init__(self, cliente, fecha_entrega, fecha_retiro, maquina, barril):
        Servicio.__init__(self, cliente, fecha_entrega, fecha_retiro, maquina, barril)
        self.tipo = 'NORMAL'

    def borrar_servicios_normales(self):
        """
        Borra todos los registros de ServicioNormal en la base de datos.
        """
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            servicios_normales = [obj for obj in dbroot.values() if isinstance(obj, ServicioNormal)]

            for servicio in servicios_normales:
                del dbroot[servicio.idservicio]  # Elimina el registro

            transaction.commit()
            db.close()
        except Exception as e:
            db.close()
            raise RuntimeError(f"Error al borrar los datos de ServicioNormal: {e}")
    def generar_id_servicio(self):
        """
        Devuelve el mayor número de orden (ID de servicio) en la base de datos.

        Returns:
            int: El ID de servicio más alto encontrado.
        """
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            ids_servicio = [obj.idservicio for obj in dbroot.values() if isinstance(obj, Servicio)]
            db.close()
            return max(ids_servicio, default=0)
        except Exception as e:
            db.close()
            raise RuntimeError(f"Error al generar el ID de servicio: {e}")


    def cargar_servicio(self, objeto, idservicio):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            dbroot[idservicio] = objeto
            transaction.commit()
            db.close()
            return idservicio
        except Exception as e:
            db.close()
            raise ('No se ha podido crear el servicio \n', e)

    def listar_servicio(self):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, ServicioNormal):
                    servicio_normal = ServicioNormal(obj.cliente, obj.fecha_entrega, obj.fecha_retiro, obj.maquina,
                                                     obj.barril)
                    servicio_normal.idservicio = obj.idservicio
                    servicio_normal.estado_servicio = obj.estado_servicio
                    servicio_normal.cantidad_barril = obj.cantidad_barril
                    result.append(servicio_normal)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise('Error al consultar la base', e)

    def buscar_servicio(self, idservicio):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = {}
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, ServicioNormal):
                    if obj.idservicio == idservicio:
                        servicio_normal = ServicioNormal(obj.cliente, obj.fecha_entrega, obj.fecha_retiro, obj.maquina,
                                                         obj.barril)
                        servicio_normal.idservicio = obj.idservicio
                        servicio_normal.estado_servicio = obj.estado_servicio
                        servicio_normal.cantidad_barril = obj.cantidad_barril
                        servicio_normal.costo_total = obj.costo_total
                        result = servicio_normal

            db.close()
            return result
        except Exception as e:
            db.close()
            raise ('Error al consultar la base', e)

    def buscar_servicio_cod(self, idservicio):
        return self.buscar_servicio(idservicio)

    def buscar_servicio_ced(self, cliente):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, ServicioNormal):
                    if obj.cliente == cliente:
                        servicio_normal = ServicioNormal(obj.cliente, obj.fecha_entrega, obj.fecha_retiro, obj.maquina, obj.barril)
                        servicio_normal.idservicio = obj.idservicio
                        servicio_normal.estado_servicio = obj.estado_servicio
                        servicio_normal.cantidad_barril = obj.cantidad_barril
                        result.append(servicio_normal)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise ('Error al consultar la base', e)

    def finalizar_servicio(self, objeto, idservicio):
        objeto.estado_servicio = 'FINALIZADA'
        return self.cargar_servicio(objeto, idservicio)

    def calcular_costo(self, idservicio):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = {}
            obj = dbroot[idservicio]
            if isinstance(obj, ServicioNormal):
                if obj.estado_servicio == 'PENDIENTE':
                    servicio_normal = ServicioNormal(obj.cliente, obj.fecha_entrega, obj.fecha_retiro, obj.maquina, obj.barril)
                    servicio_normal.idservicio = obj.idservicio
                    servicio_normal.estado_servicio = obj.estado_servicio
                    servicio_normal.cantidad_barril = obj.cantidad_barril
                    servicio_normal.costo_total = obj.barril['precio'] * obj.cantidad_barril
                    result = servicio_normal
            db.close()
            return result
        except Exception as e:
            db.close()
            raise ('Error al consultar la base', e)
