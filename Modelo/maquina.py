from Util.base import MiZODB, transaction
from persistent import Persistent

class Maquina(Persistent):
    def __init__(self, nro_maquina, peso, estado, cantidad):
        self.codigom = ''
        self.nro_maquina = nro_maquina
        self.peso = peso
        self.estado = estado
        self.cantidad = cantidad

    def carga_datos(self, codigo):
        self.codigom = codigo

    def cargar_maquina(self, obj, clave):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            dbroot[clave] = obj
            transaction.commit()
            db.close()
            return clave
        except Exception as e:
            db.close()
            raise ('No se ha podido crear \n', e)

    def listar_maquina(self):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Maquina):
                    maquina = Maquina(obj.nro_maquina, obj.peso, obj.estado, obj.cantidad)
                    maquina.codigom = obj.codigom
                    result.append(maquina)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise('Error al consultar la base', e)

    def buscar_maquina(self, codigo):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            maquina = ''
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Maquina):
                    if obj.codigom == codigo:
                        maquina = {'nro_maquina': obj.nro_maquina, 'codigom': obj.codigom,
                                   'peso': obj.peso, 'estado': obj.estado, 'cantidad': obj.cantidad}
            db.close()
            return maquina
        except Exception as e:
            db.close()

