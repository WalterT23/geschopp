# clase que guarda datos basicos de persona
from abc import ABCMeta, abstractmethod

class Contacto(metaclass=ABCMeta):

    def __init__(self, nro_celular, correo, direccion):
        self.nro_celular = nro_celular
        self.correo = correo
        self.direccion = direccion
