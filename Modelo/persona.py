# clase que guarda datos basicos de persona
from abc import ABCMeta, abstractmethod

from Modelo.contacto import Contacto


class Persona(metaclass=ABCMeta):

    def __init__(self, nombre, apellido, cedula, fecha_nac, nro_celular, correo, direccion):
        self.nombre = nombre
        self.apellido = apellido
        self.cedula = cedula
        self.fecha_nac = fecha_nac
        self.contacto = Contacto(nro_celular, correo, direccion)


    @abstractmethod
    def carga_datos(self, codigo):
        pass

    @abstractmethod
    def cargar_persona(self, obj, clave):
        pass

    @abstractmethod
    def listar_persona(self):
        pass

    @abstractmethod
    def buscar_persona(self, codigo):
        pass

    @abstractmethod
    def agregar_contacto(self, contacto):
        pass

    @abstractmethod
    def obtener_contacto(self):
        pass
