from Util.base import MiZODB, transaction
from persistent import Persistent

class Barril(Persistent):
    def __init__(self, capacidad, marca, precio):
        self.codigob = ''
        self.capacidad = capacidad
        self.marca = marca
        self.precio = precio

    def carga_datos(self, codigo):
        self.codigob = codigo

    def cargar_barril(self, obj, clave):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            dbroot[clave] = obj
            transaction.commit()
            db.close()
            return clave
        except Exception as e:
            db.close()
            raise ('No se ha podido crear \n', e)

    def listar_barril(self):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Barril):
                    barril = Barril(obj.capacidad, obj.marca, obj.precio)
                    barril.codigob = obj.codigob
                    result.append(barril)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise('Error al consultar la base', e)

    def buscar_barril(self, codigo):
        try:
            db = MiZODB('./Data.fs')
            dbroot = db.raiz
            barril = ''
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Barril):
                    if obj.codigob == codigo:
                        barril = {'capacidad': obj.capacidad, 'marca': obj.marca, 'precio': obj.precio, 'codigob': obj.codigob}
            db.close()
            return barril
        except Exception as e:
            db.close()



