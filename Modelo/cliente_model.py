from abc import ABC

from Modelo.persona import Persona
from Util.base import MiZODB, transaction
from persistent import Persistent


def buscar_persona_cedula(cedula):
    """Realiza la busqueda de la persona por cedula """
    db = MiZODB('./Data.fs')
    try:
        dbroot = db.raiz
        persona = ''
        for key in dbroot.keys():
            obj = dbroot[key]
            if isinstance(obj, Cliente):
                if obj.cedula == cedula:
                    persona = {'nombrecompleto': obj.nombre + ' ' + obj.apellido,
                               'codigo': obj.codigof, 'cedula': obj.cedula}
        db.close()
        return persona
    except Exception as e:
        db.close()
        raise ('Error', e)


class Cliente(Persona, Persistent, ABC):
    def __init__(self, nombre, apellido, cedula, fecha_nac, nro_celular, correo, direccion):
        Persona.__init__(self, nombre, apellido, cedula, fecha_nac, nro_celular, correo, direccion)
        self.codigof = ''
        self.fecha_ingreso = None

    def __str__(self):
        return '''\
    Nombre:\t\t{}
    Apellido:\t\t{}
    Cedula:\t\t{}
    Telefono:\t\t{}
    Email:\t\t{}
    Fecha_Nacimiento:\t{}
    Codigo:\t\t{}'''.format(self.nombre, self.apellido, self.cedula, self.telefono, self.email, self.fecha_nac,
                            self.codigof)

    def carga_datos(self, codigo):
        self.codigof = codigo

    def cargar_persona(self, obj, clave):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            dbroot[clave] = obj
            transaction.commit()
            db.close()
            return clave
        except Exception as e:
            db.close()
            raise ('No se ha podido crear \n', e)

    def listar_persona(self):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            result = []
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Cliente):
                    cliente = Cliente(obj.nombre, obj.apellido, obj.cedula, obj.fecha_nac, '', '', '')
                    cliente.contacto = obj.contacto
                    cliente.codigof = obj.codigof
                    result.append(cliente)
            db.close()
            return result
        except Exception as e:
            db.close()
            raise ('Error al consultar la base', e)

    def buscar_persona(self, codigo):
        db = MiZODB('./Data.fs')
        try:
            dbroot = db.raiz
            persona = ''
            for key in dbroot.keys():
                obj = dbroot[key]
                if isinstance(obj, Cliente):
                    if obj.codigof == codigo:
                        persona = {'nombrecompleto': obj.nombre + ' ' + obj.apellido, 'codigo': obj.codigof,
                                   'cedula': obj.cedula}
            db.close()
            return persona
        except Exception as e:
            db.close()
            raise ('Error', e)
